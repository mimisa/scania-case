# scania-case


## Purpose
Demo Openid connect through api with secured resource accessible with access token provided by an IDP.


## Description
The api is built using spring boot, spring security, oauth2, and kecloak as IDP.
the api connects to a local instance of keycloak backed by H2 in-memory database.

## Build
The application is dockerized, maven build is done in docker.

## Run
Docker Compose will spin up 2 containers, keycloak and spring app.

From the root of the project, run in a terminal:
```
    docker-compose up
```
## Use

Use a rest client ex. Postman to interact with the application.

POST http://localhost:8280/api/weather
- response: status 401, unauthorized

We need to provide authorization header with bearer access token.

Obtaining a token from idp:
GET http://localhost:8280/public/view-token

Copy the "access_token" from the response, and use it the authorization header

POST http://localhost:8280/api/weather
    add header Authorization: bearer <access_token>
- response 

```
{
    "todaysWeather": "rain"
}
```

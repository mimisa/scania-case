package com.code.home.scaniatest;

import static org.hamcrest.Matchers.containsString;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class AppControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldFailwith_unauthorized() throws Exception {
        mockMvc.perform(post("/api/weather").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("error").exists());
    }

    @Test
    public void shouldFailWhen_InvalidToken() throws Exception {
        mockMvc.perform(post("/api/weather")
                .header("Authorization", "bearer INVALID_TOKEN")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized())
                .andExpect(header().string("WWW-Authenticate", containsString("invalid_token")));
    }

     /*
     The method .with(jwt()) is equivalent to adding an Authorization bearer header to the request with a jwt token.
     Since the only purpose of this test it to request a resource with a valid token to pass authorization, specificities of the token, algorithm, key, claims etc. are not relevant.
     The jwt token is provided by spring security test framework:
        {
            "headers" : { "alg" : "none" },
            "claims" : {
                "sub" : "user",
                "scope" : "read"
            }
        }
    */
    @Test
    public void shouldSuccesedWhen_validToken_() throws Exception {
        mockMvc.perform(post("/api/weather").with(jwt())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.todaysWeather").value("rain"));
    }

}

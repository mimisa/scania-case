package com.code.home.scaniatest;

import java.util.Map;

import com.code.home.scaniatest.client.OpenidClient;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {

    private OpenidClient openidClient;

    public AppController(OpenidClient client) {
        this.openidClient = client;
    }

    @PostMapping("/api/weather")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, String> tellTheWeather() {
        return Map.of("todaysWeather", "rain");
    }

    @GetMapping("/public/view-token")
    @ResponseStatus(HttpStatus.OK)
    public String viewToken() {
        return openidClient.requestTokenFromIDP();
    }

}

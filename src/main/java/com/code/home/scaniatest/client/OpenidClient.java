package com.code.home.scaniatest.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Component
public class OpenidClient {

    private final String clientId;
    private final String clientSecret;
    private final String tokenEndpoint;

    @Autowired
    public OpenidClient(@Value("${client-id}") String clientId, @Value("${client-secret}") String clientSecret,
            @Value("${token-uri}") String tokenEndpoint) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.tokenEndpoint = tokenEndpoint;

    }

    public String requestTokenFromIDP() {
        RestTemplate restTemplate = new RestTemplateBuilder().build();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("client_id", clientId);
        requestBody.add("client_secret", clientSecret);
        requestBody.add("grant_type", "client_credentials");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(requestBody,
                headers);

        ResponseEntity<String> response = restTemplate.postForEntity(tokenEndpoint, request, String.class);
        return response.getBody();
    }


}
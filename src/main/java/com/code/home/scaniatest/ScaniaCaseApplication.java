package com.code.home.scaniatest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScaniaCaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScaniaCaseApplication.class, args);
	}

}
